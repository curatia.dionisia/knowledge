
## Notatki z Syllabusa ISTQB

### **Spis treści**

**Rozdział 1. Podstawy testowania**

**Rozdział 2. Testowanie w cyklu życia oprogramowania**

**Rozdział 3. Testowanie statyczne**

**Rozdział 4. Techniki testowania**

**Rozdział 5. Zarządzanie testami**

**Rozdział 6. Narzędzia wspomagające testowanie**

### Rozdział 1. Podstawy testowania 

##### Słowa kluczowe: 
**awaria** jest skutkiem defektów w oprogramowaniu. Wykonanie kodu zawierającego defekt może (ale nie musi) spowodować awarię. Nie wszystkie nieoczekiwane wyniki testów oznaczają awarię. 

**analiza testów**, 

**celem testowania** jest  ocena jakości oprogramowania i zmniejszenie ryzyka wystąpienia awarii podczas eksploatacji. 

**dane testowe**, 

**debugowanie** to czynność związana z wytwarzaniem oprogramowania, która polega na znajdowaniu, analizowaniu i usuwaniu tych defektów. Następnie wykonywane jest **testowanie potwierdzające**, które pozwala sprawdzić, czy wprowadzone poprawki w rezultacie spowodowały usunięcie defektów 

**defekt** (inaczej zwany usterką lub pluskwą) powstaje na skutek pomyłki (błędu) człowieka w kodzie oprogramowania lub w innym związanym z nim podukcie

**harmonogram wykonywania testów**, 

**implementacja testów**, 

**jakość**, 

**monitorowanie testów i nadzór nad testami**, 

**planowanie testów**, 

**podstawa testów**, 

**podstawowa przyczyna defektu** to pierwotny powód, w wyniku którego defekt ten powstał 

**pokrycie**, 

**pomyłka**  inaczej błąd człowieka. Na skutek pomyłki może powstać defekt.

**procedura testowa**, 

**projektowanie testów**, 

**przedmiot testów**, 

**przypadek testowy**, 

**śledzenie, testalia**, 

**testowanie oprogramowania** pozwala ocenić jego jakość i zmniejszyć ryzyko wystąpienia awarii podczas eksploatacji. Nieprawidłowe funkcjonowanie oprogramowania może powodować wiele problemów, w tym straty finansowe, stratę czasu, utratę reputacji firmy, a nawet utratę zdrowia lub życia. Powszechnie uważa się, że testowanie polega wyłącznie na wykonywaniu testów, czyli uruchamianiu oprogramowania i sprawdzaniu uzyskanych rezultatów. Jednak w skład procesu testowego wchodzą również takie czynności jak: planowanie, analiza, projektowanie i implementacja testów, raportowanie o postępie i wynikach testów oraz dokonywanie oceny jakości przedmiotu testów. 

**ukończenie testów**, 

**walidacja** 

**warunek testowy** 
Na początku zapoznajemy się ze specyfikacją testową i określamy, co chcemy zweryfikować (w ISTQB tak powstały dokument nazywa się warunkiem testowym) 

**weryfikacja sprawdzanie**, czy system spełnia wyspecyfikowane wymagania, historyjki użytkownika lub inne formy specyfikacji

**wykonywanie testów** czyli uruchamianiu oprogramowania i sprawdzaniu uzyskanych rezultatów 

**wyrocznia testowa**, 

**zapewnienie jakości** skupia się zazwyczaj na prawidłowym przestrzeganiu właściwych procesów w celu uzyskania pewności, że zostaną osiągnięte odpowiednie poziomy jakości. 

**zestaw testowy** 


### Rozdział 2. Testowanie w cyklu życia oprogramowania


### Rozdział 3. Testowanie statyczne

Testowanie może wymagać uruchomienia testowanego modułu lub systemu – mamy wtedy do czynienia z tzw. **testowaniem dynamicznym**. Można również wykonywać testy bez uruchamiania testowanego obiektu – takie testowanie nazywa się **testowaniem statycznym**. 

### Rozdział 4. Techniki testowania


### Rozdział 5. Zarządzanie testami 


### Rozdział 6. Narzędzia wspomagające testowanie










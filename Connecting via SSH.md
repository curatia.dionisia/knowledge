### Connecting via SSH to your server

1. check if you have ssh-agent running with the `ps x` command
2. if not, run ssh-agent using the command ``eval `ssh-agent -s` ``
3. add key `ssh-add <file_path>`
4. enter the password for the key (You remember that. You just have to remember.)
5. with `ssh-add -l` you will list identities
6. connect to SSH server`ssh username@ip.addres` or `ssh username@hostname`
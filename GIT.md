# GIT podstawowe komendy

#### `git config`
`git config--list` wyświetl bieżące ustawienia konfiguracji

#### `git init` utwórz repo

#### `git clone` utwórz kopię roboczą

`git clone username@host:/path/to/repository` utwórz kopię roboczą zdalnego repozytorium

`git clone /path/to/repository` utwórz kopię roboczą lokalnego repozytorium

#### `git status` wyświetl status zawartości

#### `git add` dodaj zmiany

`git add .` add anything that isn't ignored

`git add -A` add all

`git add --all` add all

`git add -p` używając tej komendy zobaczymy jakie zmiany zostały dokonane w pliku. Możemy je zaakceptować lub odrzucić

#### `git branch` wyświetl branche

`git branch -a ` wyświetl wszystkie branche

`git branch <branch name>` tworzenie brancha

#### `git commit` opisz zmiany
`git commit -m "your message"`

#### `git push`

`git push origin <branch name>` wypchnij zmiany

`git push -u origin master` - flaga `-u` oznacza, że ma śledzić mastera na orginie

#### `git pull` ściągnij i zaaplikuj zmiany

#### `git fetch` ściągnij zmiany

#### `git merge` aplikuj zmiany

`git merge <branch name>` łączy obie gałęzie master i nową (czyli aplikuje amiany)

#### `git checkout`

`git checkout <branch name>` przełącza pomiędzy branchami

`git checkout -- <files name>` usunięcie pliku z katalogu roboczego (wycofuje lokalne zmiany) 

`git checkout -b <branch name>` tworzy brancha 

`git checkout -` przełącza do poprzednio używanego brancha
#### `git stash`

#### `git reset`

#### `git remote`

#### `git log` wyświetla historię commitów

#### `git tag`


##JSON: JavaScript Object Notation.

JSON is a syntax for storing and exchanging data.
JSON is text, written with JavaScript object notation.
JSON uses JavaScript syntax, but the JSON format is text only.
Text can be read and used as a data format by any programming language.

*Why use JSON?*
Since the JSON format is text only, it can easily be sent to and from a server, and used as a data format by any programming language.

JavaScript has a built in function to convert a string, written in JSON format, into native JavaScript objects:

`JSON.parse()`

So, if you receive data from a server, in JSON format, you can use it like any other JavaScript object
##JSON Syntax Rules

JSON syntax is derived from JavaScript object notation syntax:

1. Data is in name/value pairs
1. Data is separated by commas
3. Curly braces hold objects
4. Square brackets hold arrays

A name/value pair consists of a field name (in double quotes), followed by a colon, followed by a value.

*JSON names require double quotes. JavaScript names don't.*

####JSON - Evaluates to JavaScript Objects
The JSON format is almost identical to JavaScript objects.

In JSON, keys must be strings, written with double quotes:

JSON
`{ "name":"John" }`

In JavaScript, keys can be strings, numbers, or identifier names:

JavaScript
`{ name:"John" }`

##JSON Values

In JSON, values must be one of the following data types:

- a string
- a number
- an object (JSON object)
- an array
- a boolean
- null

In JavaScript values can be all of the above, plus any other valid JavaScript expression, including:

- a function
- a date
- undefined

**In JSON, string values must be written with double quotes:**

*Example*
JSON
`{ "name":"John" }`

In JavaScript, you can write string values with double or single quotes:

JavaScript
`{ name:'John' }`

**JSON Numbers**
Numbers in JSON must be an integer or a floating point.

Example
`{ "age":30 }`

**JSON Objects**
Values in JSON can be objects.

Example
`{"employee":{ "name":"John", "age":30, "city":"New York" }
}`

Objects as values in JSON must follow the same rules as JSON objects.

**JSON Arrays**
Values in JSON can be arrays.

Example
`{
"employees":[ "John", "Anna", "Peter" ]
}`

**JSON Booleans**
Values in JSON can be true/false.

Example
`{ "sale":true }`

**JSON null**
Values in JSON can be null.

Example
{ "middlename":null }

**Object Syntax**
Example
`{ "name":"John", "age":30, "car":null }`

- JSON objects are surrounded by curly braces {}.
- JSON objects are written in key/value pairs.
- Keys must be strings, and values must be a valid JSON data type (string, number, object, array, boolean or null).
- Keys and values are separated by a colon.
- Each key/value pair is separated by a comma
### API

Dwa podstawowe podejścia do budowania programowego interfejsu aplikacji webowych to REST (RESTful) API i SOAP API. 

**REST** (ang. Representational State Transfer – «przeniesienie statusu widoku») zapewnia komunikację między klientem (na ogół to przeglądarka) i serwerem za pomocą zwykłych zapytań HTTP (GET, POST, PUT, DELETE itd), przekazując informację od klienta w parametrach samych zapytań, informację od serwera — w body odpowiedzi (którym może być na przykład obiekt JSON lub dokument XML). 
REST to styl architektoniczny, a nie standard.

**SOAP** (ang. Simple Object Access Protocol – prosty protokół dostępu do obiektów, aż do specyfikacji 1.2) charakteryzuje się wykorzystaniem protokołu HTTP(S) — tylko jako transportu (najczęściej, metodą POST). 
Wszystkie szczegóły wiadomości (w obie strony — od klienta do serwera i z powrotem) są przesyłane w standardowym dokumencie XML. SOAP może pracować i z innymi protokołami warstwy aplikacji (SMTP, FTP), ale najczęściej jest stosowany na HTTP(S). 
SOAP jest protokołem i ma specyfikacje.

**Korzyści z testowania API:**
1. daje dokładną informację o lokalizacji błędu
2. pozwala na oszczędność czasu przy przygotowywaniu danych testowych i scenariuszy
3. umożliwia odtworzenie testów przy dużych zbiorach danych wejściowych
4. mikroserwisy - testowanie API pozwala przekonać się o poprawności integrowania oddzielnych serwisów i w zachowaniu logiki funkcjonowania systemu



